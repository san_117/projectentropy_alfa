package components;

import gameengine2018o.engine.Converter;
import gameengine2018o.engine.IUpdatable;
import gameengine2018o.engine.Viewport;
import gameengine2018o.engine.window.WindowObject;
import gameengine2018o.engine.world.WorldObject;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import javax.imageio.ImageIO;

public class Animator  extends SpriteRenderer implements IUpdatable
{
    private int speed;
    private int frames;
    
    private int index = 0; //current frame
    private int count = 0;//last frame
    
    private ArrayList<BufferedImage> images;
    
    private boolean isFlipped;

    public Animator(WorldObject worldObject, boolean isEnable, String animationFolderPath, int speed, int drawPriority) {
        super(worldObject, drawPriority);
        
        this.isEnable = isEnable;
        
        File f = new File(animationFolderPath);
        ArrayList<File> files = new ArrayList<File>(Arrays.asList(f.listFiles()));
        
        images = new ArrayList<>();
        
        try{
            for (int i = 0; i < files.size(); i++) {
                BufferedImage frame = ImageIO.read(new File(animationFolderPath + "/" + files.get(i).getName()));
                images.add(frame);
            }
        }catch(IOException e){
            System.out.println("Can´t load animation from folder: " + animationFolderPath);
        }
        
        this.speed = speed;
        frames = images.size();
        
        this.currentImage = images.get(0);
    }
    
    public void runAnimation(){
        index++;
        if(index> speed){
            index = 0;
            nextFrame();
        }
    }
    
    public void setFlip(boolean value){
        this.isFlipped = value;
    }
    
    private void nextFrame(){
        for(int i = 0; i < frames; i++){
            if(count == i)
                currentImage = images.get(i);
            }
        
        count++;
        
        if(count > frames)
            count = 0;
        
    }

    public BufferedImage getCurrentImg() {
        return currentImage;
    }
    
    public void reestartAnimation()
    {
        index = 0;
        count = 0;
    }

    @Override
    public void Update() {
        runAnimation();
    }
    
    @Override
    public void paint(Graphics2D graphics, Viewport viewport, Dimension canvasDimension) {
       if(isEnable){
            WindowObject object = Converter.toWindow(this.getWorldObject(), viewport, canvasDimension);

            if(!isFlipped){
                graphics.drawImage(this.currentImage, (int)object.getX(), (int)object.getY(), (int)object.getWidth(), (int)object.getHeight(), null);
            }else{
                graphics.drawImage(flipHoriz(this.currentImage), (int)object.getX() + (int)object.getWidth()/2, (int)object.getY(), (int)object.getWidth(), (int)object.getHeight(), null);
            }
       }
    }
    
    private BufferedImage flipHoriz(BufferedImage image) {
        BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D gg = newImage.createGraphics();
        gg.drawImage(image, image.getHeight(), 0, -image.getWidth(), image.getHeight(), null);
        gg.dispose();
        return newImage;
    }
}
