package components;

import gameengine2018o.engine.world.WorldObject;

/**
 *
 * @author Santiago Cervera
 */
public class Behaviour {
    
    public WorldObject worldObject;
    protected boolean isEnable;

    public Behaviour(WorldObject worldObject)
    {
        this.worldObject = worldObject;
        this.worldObject.addComponent(this);
        this.isEnable = true;
    }
    
    public WorldObject getWorldObject() 
    {
        return worldObject;
    }
    
    public boolean getIsEnabled(){
        return isEnable;
    }
    
    public void setEnabled(boolean value){
        isEnable = value;
    }

}
