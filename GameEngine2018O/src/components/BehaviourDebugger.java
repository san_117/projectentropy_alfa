package components;

import gameengine2018o.engine.IUpdatable;
import gameengine2018o.engine.world.WorldObject;

public class BehaviourDebugger<T extends Behaviour> extends Behaviour implements IUpdatable{

    T inspectedElement;
    
    public BehaviourDebugger(WorldObject worldObject, T inspectElement) {
        super(worldObject);
        inspectedElement = inspectElement;
    }

    @Override
    public void Update() {
        System.out.println("Player: " + inspectedElement.toString());
    }
    
}
