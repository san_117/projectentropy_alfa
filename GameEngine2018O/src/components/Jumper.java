package components;

import gameengine2018o.engine.IUpdatable;
import gameengine2018o.engine.Input;
import gameengine2018o.engine.world.WorldObject;
import java.awt.event.KeyEvent;

public class Jumper extends Behaviour implements IUpdatable{
    
    RigidBody2D myRigidbody;
    
    private float force = 0.7f;
    private int aviableJumps = 0;
    
    private boolean keyPressed;
    
    public Jumper(WorldObject worldObject, RigidBody2D rigidbody) {
        super(worldObject);
        
        myRigidbody = rigidbody;
    }
    
    public void setAviableJumps(int value){
        this.aviableJumps = value;
    }
    
    public int getAviableJumps(){
        return this.aviableJumps;
    }

    @Override
    public void Update() {
        if(aviableJumps > 0){
            if(Input.getKeyPressed(KeyEvent.VK_SPACE)){
                if(!keyPressed){
                    myRigidbody.addVerticalForce(force);
                    aviableJumps--;
                    keyPressed = true;
                }

                if(!myRigidbody.isEnable)
                {
                    myRigidbody.setEnabled(true);
                }

                return;
            }
        
            keyPressed = false;
        }
    }
    
    
    
}
