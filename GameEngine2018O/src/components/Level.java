package components;

import gameengine2018o.MiWorldObject;
import gameengine2018o.engine.Collider;
import gameengine2018o.engine.IStartable;
import gameengine2018o.engine.IUpdatable;
import gameengine2018o.engine.Time;
import gameengine2018o.engine.world.WorldObject;
import java.util.ArrayList;


public class Level extends Behaviour implements IUpdatable, IStartable{

    ArrayList<WorldObject> blocks;
    private float speed = 0.01f;
    
    public Level(WorldObject worldObject) {
        super(worldObject);
        
        blocks = new ArrayList<>();
        
        
    }
    
    private void preset_flat(ArrayList<WorldObject> blocksList, int size){
        
        WorldObject grass_left = new MiWorldObject(0 * 10, 0, 10, 10);
        SpriteRenderer render_left = new SpriteRenderer(grass_left, "resources/blocks/grass_left.png", 1);
        Collider coll_left = new Collider(grass_left);
        blocksList.add(grass_left);
        
        for(int i = 1; i <= size - 1; i++){
            WorldObject grass = new MiWorldObject(i * 10, 0, 10, 10);
            SpriteRenderer render = new SpriteRenderer(grass, "resources/blocks/grass.png", 1);
            Collider coll = new Collider(grass);
            blocksList.add(grass);
        }
        
        WorldObject grass_right = new MiWorldObject(size * 10, 0, 10, 10);
        SpriteRenderer render = new SpriteRenderer(grass_right, "resources/blocks/grass_right.png", 1);
        Collider coll = new Collider(grass_right);
        blocksList.add(grass_right);
    }
    
    private void preset_small_hole(ArrayList<WorldObject> blocksList, int holeSize){
        
        preset_flat(blocksList, 4);
        
        for(int i = 5; i < 5 + holeSize; i++){
            WorldObject fall = new MiWorldObject(i * 10, 0, 10, 10);
            blocksList.add(fall);
        }
        
        preset_flat(blocksList, 4);
    }
    
    private void preset_flat_platform(ArrayList<WorldObject> blocksList){
        
        WorldObject grass_left = new MiWorldObject(0 * 10, 0, 10, 10);
        SpriteRenderer render_left = new SpriteRenderer(grass_left, "resources/blocks/grass_left.png", 1);
        Collider coll_left = new Collider(grass_left);
        blocksList.add(grass_left);
        
        WorldObject grass_platform = new MiWorldObject(0 * 10, 30, 30, 10);
        SpriteRenderer render_platform = new SpriteRenderer(grass_platform, "resources/blocks/platform.png", 1);
        Collider coll_render = new Collider(grass_left);
        blocksList.add(grass_platform);
        
        for(int i = 1; i <= 3 - 1; i++){
            WorldObject grass = new MiWorldObject(i * 10, 0, 10, 10);
            SpriteRenderer render = new SpriteRenderer(grass, "resources/blocks/grass.png", 1);
            Collider coll = new Collider(grass);
            blocksList.add(grass);
        }
        
        WorldObject grass_right = new MiWorldObject(3 * 10, 0, 10, 10);
        SpriteRenderer render = new SpriteRenderer(grass_right, "resources/blocks/grass_right.png", 1);
        Collider coll = new Collider(grass_right);
        blocksList.add(grass_right);
    }

    @Override
    public void Update() {
        for(WorldObject wo : blocks){
            wo.getTransform().translate(-speed * Time.getDeltaTime(), 0);
        }
    }

    @Override
    public void Start() {
        preset_flat(blocks, 5);
        preset_small_hole(blocks, 3);
        preset_flat(blocks, 5);
        preset_flat_platform(blocks);
        preset_flat(blocks, 5);
        preset_flat_platform(blocks);
        preset_small_hole(blocks, 3);
        preset_flat(blocks, 5);
        
        for (int i = 0; i < blocks.size(); i++) {
            blocks.get(i).getTransform().getLocalPosition().setX(i * 10);
        }
    }
}
