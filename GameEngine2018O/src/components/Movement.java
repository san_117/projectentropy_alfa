package components;

import gameengine2018o.engine.IUpdatable;
import gameengine2018o.engine.Input;
import gameengine2018o.engine.Time;
import gameengine2018o.engine.world.WorldObject;
import java.awt.event.KeyEvent;

public class Movement extends Behaviour implements IUpdatable{
    
    private float speed = 0.01f;
    
    public Movement(WorldObject worldObject) {
        super(worldObject);
    }

    @Override
    public void Update() {
       if(Input.getKeyPressed(KeyEvent.VK_D)){
           this.getWorldObject().getTransform().translate(speed * Time.getDeltaTime(), 0);
       }
       
       if(Input.getKeyPressed(KeyEvent.VK_A)){
           this.getWorldObject().getTransform().translate(-speed * Time.getDeltaTime(), 0);
       }
    }
}
