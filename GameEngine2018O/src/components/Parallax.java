package components;

import gameengine2018o.MiWorldObject;
import gameengine2018o.engine.IStartable;
import gameengine2018o.engine.IUpdatable;
import gameengine2018o.engine.Time;
import gameengine2018o.engine.world.WorldObject;

public class Parallax extends Behaviour implements IUpdatable, IStartable{
    
    WorldObject layers[];
    
    private float speed = 0.01f;
    
    private int mapSize = 3;
    private int layerCount = 11;
    
    public Parallax(WorldObject worldObject) {
        super(worldObject);
        
        layers = new WorldObject[11 * mapSize];
    }

    @Override
    public void Update() {
        for(int i = 0; i < mapSize; i++){
            for(int e = 0; e < layerCount; e++){
                layers[e + (i * layerCount)].getTransform().translate(-speed * e, 0);
            }
        }
    }

    @Override
    public void Start() {
        int width = 200;
        int height = 200;

        for(int i = 0; i < mapSize ; i++){
            layers[0 + (i * layerCount)] = new MiWorldObject(width * i, 0, width, height);
            SpriteRenderer sprite_0 = new SpriteRenderer(layers[0 + (i * layerCount)], "resources/skyboxes/Layer_0010_1.png", -1);

            layers[1 + (i * layerCount)] = new MiWorldObject(width * i, 0, width , height);
            SpriteRenderer sprite_1 = new SpriteRenderer(layers[1 + (i * layerCount)], "resources/skyboxes/Layer_0009_2.png", -1);

            layers[2 + (i * layerCount)] = new MiWorldObject(width * i, 0, width , height);
            SpriteRenderer sprite_2 = new SpriteRenderer(layers[2 + (i * layerCount)], "resources/skyboxes/Layer_0008_3.png", -1);

            layers[3 + (i * layerCount)] = new MiWorldObject(width * i, 0, width , height);
            SpriteRenderer sprite_3 = new SpriteRenderer(layers[3 + (i * layerCount)], "resources/skyboxes/Layer_0007_Lights.png", -1);

            layers[4 + (i * layerCount)] = new MiWorldObject(width * i, 0, width , height);
            SpriteRenderer sprite_4 = new SpriteRenderer(layers[4 + (i * layerCount)], "resources/skyboxes/Layer_0006_4.png", -1);

            layers[5 + (i * layerCount)] = new MiWorldObject(width * i, 0, width , height);
            SpriteRenderer sprite_5 = new SpriteRenderer(layers[5 + (i * layerCount)], "resources/skyboxes/Layer_0005_5.png", -1);

            layers[6 + (i * layerCount)] = new MiWorldObject(width * i, 0, width , height);
            SpriteRenderer sprite_6 = new SpriteRenderer(layers[6 + (i * layerCount)], "resources/skyboxes/Layer_0004_Lights.png", -1);

            layers[7 + (i * layerCount)] = new MiWorldObject(width * i, 0, width , height);
            SpriteRenderer sprite_7 = new SpriteRenderer(layers[7 + (i * layerCount)], "resources/skyboxes/Layer_0003_6.png", -1);

            layers[8 + (i * layerCount)] = new MiWorldObject(width * i, 0, width , height);
            SpriteRenderer sprite_8 = new SpriteRenderer(layers[8 + (i * layerCount)], "resources/skyboxes/Layer_0002_7.png", -1);

            layers[9 + (i * layerCount)] = new MiWorldObject(width * i, 0, width , height);
            SpriteRenderer sprite_9 = new SpriteRenderer(layers[9 + (i * layerCount)], "resources/skyboxes/Layer_0001_8.png", -1);

            layers[10 + (i * layerCount)] = new MiWorldObject(width * i, 0, width , height);
            SpriteRenderer sprite_10 = new SpriteRenderer(layers[10 + (i * layerCount)], "resources/skyboxes/Layer_0000_9.png", -1);
        }
    }
}
