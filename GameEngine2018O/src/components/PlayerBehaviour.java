package components;

import gameengine2018o.engine.Collider;
import gameengine2018o.engine.IUpdatable;
import gameengine2018o.engine.Input;
import gameengine2018o.engine.world.WorldObject;
import java.awt.event.KeyEvent;

public class PlayerBehaviour extends Behaviour implements IUpdatable{

    RigidBody2D rigidBody;
    Jumper jumper;
    Collider collider;
    
    Animator idle_anim;
    Animator movement_anim;
    Animator jump_anim;
    Animator falling_anim;
    
    boolean isFlipped;
    
    public PlayerBehaviour(WorldObject worldObject) {
        super(worldObject);
        
        idle_anim = new Animator(this.getWorldObject(), true, "resources/animations/player/run", 8, 1);
        movement_anim = new Animator(this.getWorldObject(), false, "resources/animations/player/run", 8, 1);
        jump_anim = new Animator(this.getWorldObject(), false, "resources/animations/player/jump", 12, 1);
        falling_anim = new Animator(this.getWorldObject(), false, "resources/animations/player/fall", 8, 1);
        
        rigidBody = new RigidBody2D(this.getWorldObject(), 1);
        Movement movement = new Movement(this.getWorldObject());
        jumper = new Jumper(this.getWorldObject(), rigidBody);
        collider = new Collider(this.getWorldObject());
    }

    @Override
    public void Update() {
        
        if(rigidBody.getVelocity().getY() > 0 ){
            collider.setIgnoreColissions(true);
        }else{
            collider.setIgnoreColissions(false);
        }
        
        if(Input.getKeyPressed(KeyEvent.VK_D))
        {
            isFlipped = false;
        }
        
        if(Input.getKeyPressed(KeyEvent.VK_A))
        {
            isFlipped = true;
        }
                
        if(Input.getKeyPressed(KeyEvent.VK_SPACE)){
            if(jumper.getAviableJumps() > 0){
                idle_anim.setEnabled(false);
                movement_anim.setEnabled(false);
                jump_anim.setEnabled(true);
                falling_anim.setEnabled(false);
                jump_anim.setFlip(isFlipped);
            }
        }
        
        if(!rigidBody.getIsEnabled()){
            if(Input.getKeyPressed(KeyEvent.VK_D)){
                isFlipped = false;

                idle_anim.setEnabled(false);

                movement_anim.setEnabled(true);
                movement_anim.setFlip(isFlipped);
                jump_anim.setEnabled(false);
                falling_anim.setEnabled(false);
                return;
            }

            if(Input.getKeyPressed(KeyEvent.VK_A)){
                isFlipped = true;

                idle_anim.setEnabled(false);

                movement_anim.setEnabled(true);
                movement_anim.setFlip(isFlipped);
                jump_anim.setEnabled(false);
                falling_anim.setEnabled(false);
                return;
            }

            idle_anim.setEnabled(true);
            idle_anim.setFlip(isFlipped);
            jump_anim.setEnabled(false);
            movement_anim.setEnabled(false);
            falling_anim.setEnabled(false);
            
        }else{
            
            idle_anim.setEnabled(false);
            movement_anim.setEnabled(false);
            jump_anim.setEnabled(false);
            falling_anim.setEnabled(true);
            falling_anim.setFlip(isFlipped);
            
            if(rigidBody.getVelocity().getY() < 0){
                idle_anim.setEnabled(false);
                movement_anim.setEnabled(false);
                jump_anim.setEnabled(false);
                falling_anim.setEnabled(true);
                falling_anim.setFlip(isFlipped);
            }
        }
    }
    
    public void touchGround(){
        rigidBody.setEnabled(false);
        jumper.setAviableJumps(1);
    }
    
}
