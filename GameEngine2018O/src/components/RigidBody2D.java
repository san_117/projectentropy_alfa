package components;

import gameengine2018o.engine.IUpdatable;
import gameengine2018o.engine.Physics;
import gameengine2018o.engine.Time;
import gameengine2018o.engine.world.Vector2;
import gameengine2018o.engine.world.WorldObject;

public class RigidBody2D extends Behaviour implements IUpdatable{
    
    private Vector2 velocity;
    private float mass = 1;
    private float gravityScale = 0.00005f;
    private boolean isKinematic;

    public RigidBody2D(WorldObject worldObject, float mass) {
        super(worldObject);
        this.mass = mass;
        velocity = new Vector2(0, 0);
    }
    
    public void resetVelocity(){
        worldObject.getTransform().getPosition().setY(worldObject.getTransform().getPosition().getY() + 0.001f);
        velocity = new Vector2(0, 0);
    }
    
    @Override
    public void setEnabled(boolean value){
        this.isEnable = value;
        
        if(!this.isEnable)
            resetVelocity();
    }
    
    public void setKinematic(boolean value){
        isKinematic = value;
        
        if(isKinematic)
            resetVelocity();
    }
    
    public boolean getKinematic(){
        return isKinematic;
    }
    
    public Vector2 getVelocity(){
        return this.velocity;
    }
    
    public void addVerticalForce(float force){
        velocity.setY(velocity.getY() + force);
    }
    
    public void addHorizontalForce(float force){
        velocity.setX(velocity.getX() + force);
    }
    
    @Override
    public void Update() {
        if(!isKinematic){
            velocity.setY(velocity.getY() + ((Physics.getGravity() * gravityScale) * Time.getDeltaTime()));
             getWorldObject().getTransform().translate(0, velocity.getY());
        }
    }
}
