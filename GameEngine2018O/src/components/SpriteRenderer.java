package components;

import gameengine2018o.engine.Converter;
import gameengine2018o.engine.Drawable;
import gameengine2018o.engine.Renderer;
import gameengine2018o.engine.Viewport;
import gameengine2018o.engine.window.WindowObject;
import gameengine2018o.engine.world.WorldObject;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class SpriteRenderer extends Renderer implements Drawable
{    
    protected BufferedImage currentImage;

    public SpriteRenderer(WorldObject worldObject, String imagePath, int drawPriority)
    {
        super(worldObject, drawPriority);
        
        try{
            this.currentImage = ImageIO.read(new File(imagePath));
       }catch(IOException e){
           System.out.println("Can´t load image from path: " + imagePath);
       }
    }
    
    public SpriteRenderer(WorldObject worldObject, int drawPriority)
    {
        super(worldObject, drawPriority);
        this.currentImage = null;
    }
    
    public BufferedImage getImage()
    {
        return currentImage;
    }

    public void setImage(BufferedImage image)
    {
        this.currentImage = image;
    }  

    @Override
    public void paint(Graphics2D graphics, Viewport viewport, Dimension canvasDimension) {
        if(isEnable){
            WindowObject object = Converter.toWindow(this.getWorldObject(), viewport, canvasDimension);
        graphics.drawImage(this.currentImage, (int)object.getX(), (int)object.getY(), (int)object.getWidth(), (int)object.getHeight(), null);
        }
    }
}
