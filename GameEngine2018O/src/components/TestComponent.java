package components;

import gameengine2018o.engine.IStartable;
import gameengine2018o.engine.IUpdatable;
import gameengine2018o.engine.world.WorldObject;

public class TestComponent extends Behaviour implements IUpdatable, IStartable{

    public TestComponent(WorldObject worldObject) {
        super(worldObject);
    }

    @Override
    public void Update() {
        System.out.println("components.TestComponent.Update()");
    }

    @Override
    public void Start() {
        System.out.println("components.TestComponent.Start()");
    }
    
}
