/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameengine2018o;

import gameengine2018o.engine.Engine;
import gameengine2018o.engine.Scene;
import gameengine2018o.engine.Layer;
import gameengine2018o.engine.world.WorldObject;
import components.Animator;
import components.Jumper;
import components.Level;
import components.Movement;
import components.Parallax;
import components.RigidBody2D;
import components.SpriteRenderer;
import gameengine2018o.engine.Collider;
/**
 *
 * @author MAINPC
 */
public class GameScene extends Scene{
    
    public GameScene(Engine engine) {
        super(engine);
    }
    
    @Override
    public void buildScene(){
        Layer background = new Layer();
        Layer frontground = new Layer();
        
        background.add(createBackground());
        frontground.add(new Player(0, 50, 25, 25));
        createLevel(frontground);

        super.add(background);
        super.add(frontground);
    }
    
    private void createLevel(Layer layer){
        
        WorldObject levelObject = new MiWorldObject(0, 0, 1, 1);
        Level level = new Level(levelObject);
        
        layer.add(levelObject);
    }
    
    private WorldObject createBackground(){
        
        int width = 200;
        int height = 200;
        
        WorldObject background = new MiWorldObject(0, 0, width , height);
        Parallax parallax = new Parallax(background);
        
        return background;
    }
}

