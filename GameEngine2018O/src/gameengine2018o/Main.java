package gameengine2018o;

import gameengine2018o.engine.Engine;

public class Main
{

    public static void main(String[] args)
    {
        Engine engine = new MyRunner();
        engine.start();
    }
    
}
