package gameengine2018o;

import gameengine2018o.engine.world.Transform;
import gameengine2018o.engine.world.WorldObject;

/**
 *
 * @author Pablo Rojas
 */
public class MiWorldObject extends WorldObject
{
    public MiWorldObject(double x, double y, double width, double height)
    {
        super(x, y, width, height);
    }
    
    public MiWorldObject(double x, double y, double width, double height, Transform parent)
    {      
        super(x, y, width, height, parent);
    }
}
