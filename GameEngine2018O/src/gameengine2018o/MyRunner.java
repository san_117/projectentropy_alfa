package gameengine2018o;
import gameengine2018o.engine.Engine;
import gameengine2018o.engine.Viewport;
import java.awt.Dimension;

/**
 *
 * @author Pablo Rojas
 */
public class MyRunner extends Engine
{   
    public MyRunner()
    {
        super(new Viewport(0, 0, 100, 100), new Dimension(600, 600));
        
        super.setScene(new GameScene(this));   
    }
}
