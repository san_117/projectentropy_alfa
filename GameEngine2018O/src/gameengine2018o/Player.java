package gameengine2018o;

import components.Animator;
import components.BehaviourDebugger;
import components.Jumper;
import components.Movement;
import components.PlayerBehaviour;
import components.RigidBody2D;
import gameengine2018o.engine.Collider;
import gameengine2018o.engine.Collision;
import gameengine2018o.engine.world.WorldObject;


public class Player extends WorldObject
{    
    PlayerBehaviour myBehaviour;
    
    public Player(double x, double y, double width, double height) {
        super(x, y, width, height);
        
        myBehaviour = new PlayerBehaviour(this);
        //BehaviourDebugger inspector = new BehaviourDebugger(this, true, collider);
    }
    
    @Override
    public void OnCollisionEnter(Collision coll){
        myBehaviour.touchGround();
    }
}
