package gameengine2018o.engine;

public interface Animatable
{
    public boolean isStarted();
    public void start(long timestamp);
    public boolean isAlive();
    public void update(long timestamp);
    
}
