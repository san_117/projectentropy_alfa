package gameengine2018o.engine;

import components.Behaviour;
import gameengine2018o.engine.world.Vector2;
import gameengine2018o.engine.world.WorldObject;
import java.awt.geom.Rectangle2D;

public class Collider extends Behaviour implements ICollisionable, IUpdatable
{
    Rectangle2D.Double bounds;
    private boolean ignoreColissions;
    
    public Collider(WorldObject worldObject) {
        super(worldObject);
        updateColliderPos();
    }

    public Rectangle2D.Double getBounds() 
    {
        return bounds;
    }
   
    public Vector2 Center(WorldObject object)
    {
        return new Vector2((object.getTransform().getPosition().getX() + object.getTransform().getScale().getX())/2 ,
        (object.getTransform().getPosition().getY() + object.getTransform().getScale().getY())/2);
    }
    
    private void updateColliderPos(){
        double o1_X = worldObject.getTransform().getPosition().getX();
        double o1_Y = worldObject.getTransform().getPosition().getY();
        double o1_W = worldObject.getTransform().getScale().getX();
        double o1_H = worldObject.getTransform().getScale().getY();
        this.bounds = new Rectangle2D.Double(o1_X,o1_Y,o1_W,o1_H);
    }
    
    public boolean getIgnoreColissions(){
        return ignoreColissions;
    }
    
    public void setIgnoreColissions(boolean value){
        this.ignoreColissions = value;
    }

    @Override
    public boolean isCollisioned(Collider target) {
        
        if(ignoreColissions)
            return false;
        
        return (bounds.intersects(target.getBounds()));
    }

    @Override
    public String toString() {
       return "Bounds: " + bounds.toString();
    }
    
    @Override
    public void Update() {
        updateColliderPos();
    }
}
