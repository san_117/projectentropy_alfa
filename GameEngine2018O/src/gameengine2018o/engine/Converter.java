package gameengine2018o.engine;

import gameengine2018o.engine.window.WindowObject;
import gameengine2018o.engine.world.Vector2;
import gameengine2018o.engine.world.WorldObject;
import java.awt.Dimension;

public class Converter
{
    public static Vector2 toWindow(Vector2 worldPoint, Viewport viewport, Dimension canvasDimension)
    {
        int x = toWindowXAxis(worldPoint.getX(), viewport, canvasDimension);
        int y = toWindowYAxis(worldPoint.getY(), viewport, canvasDimension);
        return new Vector2(x, y);
    }
    
    //Revisar si corresponde al esquema de pintado
    public static int toWindowXAxis(double x, Viewport viewport, Dimension canvasDimension)
    {
        double wWindow = canvasDimension.width;
        double wViewport = viewport.getxMax() - viewport.getxMin();
        double xMinWiewport = viewport.getxMin();
        return (int)( ((x - xMinWiewport)/(wViewport))*wWindow ) ;
    }
    
    //Revisar si corresponde al esquema de pintado
    public static int toWindowYAxis(double y, Viewport viewport, Dimension canvasDimension)
    {
        double hViewport = viewport.getyMax() - viewport.getyMin();
        double hWindow = canvasDimension.height;
        double yMinViewport = viewport.getyMin();
        return (int)( hWindow - (((y - yMinViewport)/(hViewport))*hWindow) );
    }
    
    private static int toWindowXMagnitude( double worldWidth, Viewport viewport, Dimension canvasDimension )
    {
        return (int)(worldWidth*canvasDimension.getWidth()/viewport.getWidth());
    }
    
    private static int toWindowYMagnitude( double worldHeight, Viewport viewport, Dimension canvasDimension )
    {
        return (int)(worldHeight*canvasDimension.getHeight()/viewport.getHeight());
    }
    
    public static WindowObject toWindow(WorldObject object, Viewport viewport, Dimension canvasDimension)
    {
        Vector2 origen = Converter.toWindow(new Vector2(object.getTransform().getPosition().getX(), object.getTransform().getPosition().getY()), viewport, canvasDimension);
        int width = Converter.toWindowXMagnitude(object.getTransform().getScale().getX(), viewport, canvasDimension);
        int height = Converter.toWindowYMagnitude(object.getTransform().getScale().getY(), viewport, canvasDimension);
        return new WindowObject(origen.getX(), origen.getY() - height, width, height);
    }
}
