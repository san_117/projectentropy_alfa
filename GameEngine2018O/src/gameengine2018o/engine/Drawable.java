package gameengine2018o.engine;

import java.awt.Dimension;
import java.awt.Graphics2D;

/**
 *
 * @author Pablo Rojas
 */
public interface Drawable extends Comparable<Object>
{
    public void paint(Graphics2D graphics, Viewport viewport, Dimension canvasDimension);
}
