package gameengine2018o.engine;

import gameengine2018o.engine.window.JMyFrame;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public abstract class Engine
{
    private JMyFrame frame;
    private boolean running;
    
    private GraphicsDevice graphicsDevice;
    private boolean fullscreen;
    private Viewport viewport;
    private Dimension canvasDimension;
    
    private Input input;    
    private Time timer;
    private Physics physics;
    private Scene currentScene; //List in future
    
    BufferedImage banner;
    private boolean initEngine;
    
    public Engine(Viewport viewport, Dimension canvasDimension)
    {
        GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        
        this.viewport = viewport;
        this.canvasDimension = canvasDimension;
        this.fullscreen = false;
        this.graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
        
        this.frame = new JMyFrame(this);
        this.frame.setCanvasDimension(canvasDimension);
        this.running = false;
        
        this.timer = new Time();
        this.physics = new Physics();
        this.input = new Input();
        
        try{
            this.banner = ImageIO.read(new File("resources/branding/banner.png"));
       }catch(IOException e){
           System.out.println("Can´t load image from path: resources/branding/banner.png");
       }
    }
    
    public void setScene(Scene scene)
    {
        currentScene = scene;
    }
    
    public Scene getCurrentScene()
    {
        return currentScene;
    }
    
    public Dimension getCanvasDimension()
    {
        return canvasDimension;
    }

    public void setCanvasDimension(Dimension canvasDimension)
    {
        this.canvasDimension = canvasDimension;
    }
    
    public Viewport getViewport()
    {
        return this.viewport;
    }

    public boolean isRunning()
    {
        synchronized(this)
        {
            return running;
        }
    }

    public void setRunning(boolean running)
    {
        synchronized(this)
        {
            this.running = running;
        }
    }
    
    public Physics getPhysics(){
        return physics;
    }
    
    public void start()
    {
        this.showWindowedFrame();
        
        
        
        this.setRunning(true);
        while(this.isRunning())
        {
            Graphics2D buffer = this.frame.getBuffer();

            timer.update();

            long t1 = System.currentTimeMillis();
            
            
            this.render(buffer);
            
            
            while(!initEngine){
                buffer.drawImage(banner, 0, 0, 600, 600, null);
                this.frame.update();
                currentScene.initScene();
                this.frame.update();
                initEngine = true;
            }
            
            currentScene.updateScene();
            
            this.frame.update();
            long t2 = System.currentTimeMillis();

            

            long diff = t2 - t1;
            long time = 16 - diff;
            if( time <= 0 )
            {
                time = 1;
            }

            try
            {
                Thread.sleep(time);
            }
            catch (InterruptedException ex){}
        }
    }
    
    private void showFullScreenFrame()
    {
        if(this.graphicsDevice.isFullScreenSupported())
        {
            this.frame.setVisible(false);
            this.frame.dispose();
            this.frame.setUndecorated(true);
            this.frame.setResizable(false);
            this.frame.setAlwaysOnTop(true);
            
            DisplayMode mode = new DisplayMode(1366, 768, 32, 60);
            this.graphicsDevice.setFullScreenWindow(this.frame);
            this.graphicsDevice.setDisplayMode(mode);
            
            this.frame.validate();
            this.fullscreen = true;
        }
        else
        {
            this.showWindowedFrame();
        }
    }
    
    private void showWindowedFrame()
    {
        this.frame.setVisible(false);
        this.frame.dispose();
        this.frame.setUndecorated(false);
        this.frame.setVisible(true);
        this.frame.setResizable(false);
        this.frame.setAlwaysOnTop(false);
        
        this.frame.setCanvasDimension(canvasDimension);
        this.frame.validate();
        this.fullscreen = false;
    }
    
    protected void render(Graphics2D buffer)
    {
        buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        buffer.clearRect(0, 0, (int)canvasDimension.getWidth(), (int)canvasDimension.getHeight());
        currentScene.render(buffer, viewport, canvasDimension);
    }
}
