package gameengine2018o.engine;

public interface ICollisionable {
    
    public boolean isCollisioned(Collider target);
}
