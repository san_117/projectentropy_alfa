package gameengine2018o.engine;

/**
 *
 * @author Santiago Cervera
 */
public interface IStartable {
    
    void Start();
}
