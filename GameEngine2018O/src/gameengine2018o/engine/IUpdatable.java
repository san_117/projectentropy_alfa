package gameengine2018o.engine;

/**
 *
 * @author Santiago Cervera
 */
public interface IUpdatable {
    
    void Update();
}
