package gameengine2018o.engine;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class Input implements KeyEventDispatcher, KeyListener
{
    private static ArrayList<KeyEvent> lastKeysPressed;
    
    public Input(){
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
        lastKeysPressed = new ArrayList<>();
    }
    
    public static boolean getKeyPressed (int e)
    {
        if(lastKeysPressed == null)
            return false;
                
        for(KeyEvent k : lastKeysPressed){
            if(e == k.getKeyCode()){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        switch(e.getID())
        {
            case KeyEvent.KEY_PRESSED : this.keyPressed(e);  break;
            case KeyEvent.KEY_RELEASED: this.keyReleased(e); break;
        }
        return true;    
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        
        for(KeyEvent k : lastKeysPressed){
            if(e.getKeyCode() == k.getKeyCode()){
               return;
            }
        }
        
        lastKeysPressed.add(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        for(KeyEvent k : lastKeysPressed){
            if(e.getKeyCode() == k.getKeyCode()){
               lastKeysPressed.remove(k);
               return;
            }
        }
    }
 }
