package gameengine2018o.engine;

import gameengine2018o.engine.world.WorldObject;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Pablo Rojas
 */
public class Layer implements Iterable<WorldObject>
{
    private ArrayList<WorldObject> objects;
    
    public Layer()
    {
        this.objects = new ArrayList<>();
    }

    public int size()
    {
        return objects.size();
    }

    public WorldObject get(int index)
    {
        return objects.get(index);
    }

    public boolean add(WorldObject e)
    {
        return objects.add(e);
    }

    public boolean remove(WorldObject o)
    {
        return objects.remove(o);
    }

    @Override
    public Iterator<WorldObject> iterator()
    {
        return objects.iterator();
    }
}
