package gameengine2018o.engine;

import components.RigidBody2D;
import gameengine2018o.engine.world.WorldObject;
import java.util.ArrayList;

public class Physics {
    
    private static final double gravity = -9.81;
    
    public static double getGravity(){
        return gravity;
    }
    
    public void updatePhysics(ArrayList<ICollisionable> collisionables){
        for(ICollisionable coll_1 : collisionables){
            for(ICollisionable coll_2 : collisionables){
                if(coll_1 != coll_2){
                    //System.out.println("updatePhysics: " + coll_1.isCollisioned((Collider)coll_2));
                    if(coll_1.isCollisioned((Collider)coll_2)){
                        
                        WorldObject wo_1 = ((Collider)coll_1).getWorldObject();
                        WorldObject wo_2 = ((Collider)coll_2).getWorldObject();
                        
                        wo_1.OnCollisionEnter(new Collision((Collider)coll_2));
                        wo_2.OnCollisionEnter(new Collision((Collider)coll_1));
                        
                        try{
                            ((RigidBody2D)wo_1.getComponent(RigidBody2D.class)).resetVelocity();
                            
                        }catch(Exception e){}
                        
                        try{
                            ((RigidBody2D)wo_2.getComponent(RigidBody2D.class)).resetVelocity();
                            
                        }catch(Exception e){}
                    }
                }
            }
        }
    }
}
