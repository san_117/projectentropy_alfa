package gameengine2018o.engine;

import components.Behaviour;
import gameengine2018o.engine.world.WorldObject;

public class Renderer extends Behaviour implements Comparable<Object>
{
    private int drawPriority = 0;
    
    public Renderer(WorldObject worldObject, int drawPriority)
    {
        super(worldObject);
        
        this.drawPriority = drawPriority;
    }
    
    public void setDrawPriority(int drawPriority) {
        this.drawPriority = drawPriority;
    }

    public int getDrawPriority() {
        return drawPriority;
    }

    @Override
    public int compareTo(Object o) {
        if(this.drawPriority < (((Renderer)o).drawPriority)){
            return -1;
        }
        
        if(this.drawPriority > (((Renderer)o).drawPriority)){
            return 1;
        }
        
        return 0;
    }
}
