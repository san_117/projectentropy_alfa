package gameengine2018o.engine;

import components.Behaviour;
import gameengine2018o.engine.world.WorldObject;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 *
 * @author Pablo Rojas
 */
public abstract class Scene implements Iterable<Layer>
{
    private ArrayList<Layer> hierarchy;
    private ArrayList<Behaviour> scene_components;
    private ArrayList<IStartable> start_components;
    private ArrayList<IUpdatable> update_components;
    private ArrayList<Drawable> drawable_components;
    private ArrayList<ICollisionable> colisionable_components;
    
    static Engine engine;
    
    public Scene(Engine engine)
    {
        this.hierarchy = new ArrayList<>();
        this.scene_components = new ArrayList<>();
        this.start_components = new ArrayList<>();
        this.update_components = new ArrayList<>();
        this.drawable_components = new ArrayList<>();
        this.colisionable_components = new ArrayList<>();
        
        Scene.engine = engine;
    }
    
    public void initScene()
    {    
        buildScene();
        
        for(int i = hierarchy.size() - 1 ; i >= 0; i--){
            for(WorldObject wo : hierarchy.get(i)){
                wo.OnStart(scene_components);
            }
        }
        
        startScene();
    }
    
    public void startScene(){
        for(IStartable component : start_components){
            component.Start();
        }
    }
    
    public void updateScene(){
        
        engine.getPhysics().updatePhysics(colisionable_components);
        
        for(IUpdatable component : update_components){
            
            Behaviour cast = (Behaviour)component;
            if(cast.getWorldObject().isIsActive()){
                if(cast.getIsEnabled()){
                    component.Update();
                }
            }
        }
    }
    
    public int count()
    {
        return hierarchy.size();
    }

    public Layer get(int index)
    {
        return hierarchy.get(index);
    }

    private void findInterfaces(ArrayList<Behaviour> behaviours){
        for(Behaviour component : behaviours){
               if(component instanceof IUpdatable)
                   update_components.add((IUpdatable)component);
               
               if(component instanceof IStartable)
                   start_components.add((IStartable)component);
               
               if(component instanceof Drawable)
                   drawable_components.add((Drawable)component);
              
               if(component instanceof ICollisionable)
                   colisionable_components.add((ICollisionable)component);
        }
        
        reorderDrawables();
    }
    
    private void findInterfaces(Behaviour component){
        if(component instanceof IUpdatable)
            update_components.add((IUpdatable)component);

        if(component instanceof IStartable)
            start_components.add((IStartable)component);

        if(component instanceof Drawable)
            drawable_components.add((Drawable)component);
        
        if(component instanceof ICollisionable)
            colisionable_components.add((ICollisionable)component);
        
        reorderDrawables();
    }
    
    private void reorderDrawables(){
        Collections.sort(drawable_components);
    }
    
    public void add(Layer e)
    {
       ArrayList<Behaviour> aux = new ArrayList<>();
       
       for(WorldObject wo : e){
           wo.OnStart(aux);
       }
       
       findInterfaces(aux);
    }
    
    public void add(WorldObject e)
    {
       ArrayList<Behaviour> aux = new ArrayList<>();
       
       e.OnStart(aux);
       
       findInterfaces(aux);
    }
    
    public void add(Behaviour e)
    {
       findInterfaces(e);
    }

    public Layer remove(int index)
    {
        return hierarchy.remove(index);
    }

    @Override
    public Iterator<Layer> iterator()
    {
        return hierarchy.iterator();
    }
    
    public static Scene getCurrentScene(){
        return engine.getCurrentScene();
    }
    
    public void render(Graphics2D buffer, Viewport viewport, Dimension canvasDimension){
        
        for (Drawable drawable : drawable_components)
        {
            drawable.paint(buffer, viewport, canvasDimension);
        } 
    }
    
    public abstract void buildScene();
}
