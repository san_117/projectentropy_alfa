package gameengine2018o.engine;


public class Time {
    
    private static long currentTime;
    private static long lastTime;
    private static long deltaTime;
    
    public static long getTime(){
        return currentTime;
    }
    
    public static long getDeltaTime(){
        return Math.max(0, Math.min(20, deltaTime));
    }
    
    public void update(){
        currentTime = System.currentTimeMillis();
        
        deltaTime =  currentTime - lastTime;
       
        lastTime = currentTime;
    }
    
}
