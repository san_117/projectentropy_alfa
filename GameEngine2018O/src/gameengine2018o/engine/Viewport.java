package gameengine2018o.engine;

public class Viewport
{
    private double xMin;
    private double xMax;
    private double yMin;
    private double yMax;

    public Viewport(double xMin, double yMin, double xMax, double yMax)
    {
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;
    }

    public double getxMin()
    {
        return xMin;
    }

    public void setxMin(double xMin)
    {
        this.xMin = xMin;
    }

    public double getxMax()
    {
        return xMax;
    }

    public void setxMax(double xMax)
    {
        this.xMax = xMax;
    }

    public double getyMin()
    {
        return yMin;
    }

    public void setyMin(double yMin)
    {
        this.yMin = yMin;
    }

    public double getyMax()
    {
        return yMax;
    }

    public void setyMax(double yMax)
    {
        this.yMax = yMax;
    }

    public double getWidth()
    {
        return this.xMax - this.xMin;
    }
    
    public double getHeight()
    {
        return this.yMax - this.yMin;
    }
    
    
    
    
}
