/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameengine2018o.engine.window;

import gameengine2018o.engine.Engine;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferStrategy;
import javax.swing.JFrame;

/**
 *
 * @author Pablo Rojas
 */
public class JMyFrame extends JFrame implements WindowListener
{
    private final Engine engine;
    
    private final GraphicsDevice graphicsDevice;
    private final Canvas canvas;
    private BufferStrategy strategy;
    private boolean initialized;
    private boolean fullscreen;
    private Dimension canvasDimension;
    
    
    public JMyFrame(Engine engine)
    {
        GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        this.graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
        this.engine = engine;
        this.canvas = new Canvas();
        this.canvasDimension = new Dimension(800, 600);
        
        super.getContentPane().add(this.canvas);
        super.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        super.addWindowListener(this);
        super.setUndecorated(true);
        
    }
    
    private void showFullScreenFrame()
    {
        if(this.graphicsDevice.isFullScreenSupported())
        {
            this.setVisible(false);
            this.dispose();
            this.setUndecorated(true);
            this.setResizable(false);
            this.setAlwaysOnTop(true);
            
            DisplayMode mode = new DisplayMode(1366, 768, 32, 60);
            this.graphicsDevice.setFullScreenWindow(this);
            this.graphicsDevice.setDisplayMode(mode);
            
            this.validate();
            this.fullscreen = true;
        }
        else
        {
            this.showWindowedFrame();
        }
    }
    
    private void showWindowedFrame()
    {
        this.setVisible(false);
        this.dispose();
        this.setUndecorated(false);
        this.setVisible(true);
        this.setResizable(false);
        this.setAlwaysOnTop(false);
        
        this.setCanvasDimension(canvasDimension);
        this.pack();
        this.validate();
        this.fullscreen = false;
    }
    
    
    public void setCanvasDimension(Dimension dimension)
    {
        this.canvas.setSize(dimension);
        super.pack();
    }

    public Graphics2D getBuffer()
    {
        if(!this.initialized)
        {
            this.canvas.createBufferStrategy(2);
            this.strategy = this.canvas.getBufferStrategy();
            this.initialized = true;
        }
        
        return (Graphics2D)this.strategy.getDrawGraphics();
    }
    
    public void update()
    {
        this.strategy.show();
    }

    public int getCanvasHeight()
    {
        return this.canvas.getHeight();
    }

    public int getCanvasWidth()
    {
        return this.canvas.getWidth();
    }

    @Override
    public void windowOpened(WindowEvent e)
    {
    }

    @Override
    public void windowClosing(WindowEvent e)
    {
        this.engine.setRunning(false);
    }

    @Override
    public void windowClosed(WindowEvent e)
    {
    }

    @Override
    public void windowIconified(WindowEvent e)
    {
    }

    @Override
    public void windowDeiconified(WindowEvent e)
    {
    }

    @Override
    public void windowActivated(WindowEvent e)
    {
    }

    @Override
    public void windowDeactivated(WindowEvent e)
    {
    }
    
}
