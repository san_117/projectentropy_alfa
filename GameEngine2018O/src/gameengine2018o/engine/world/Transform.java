package gameengine2018o.engine.world;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Santiago Cervera
 */
public class Transform implements Iterable<Transform>{
    
    private Vector2 position;
    private float rotation;
    private Vector2 scale;

    private ArrayList<Transform> childs;
    private WorldObject worldObject;
    
    public Transform(WorldObject worldObject, double x, double y, double width, double height, float angle){
        position = new Vector2(x,y);
        scale = new Vector2(width, height);
        rotation = angle;
        childs = new ArrayList<>();
        this.worldObject = worldObject;
    }
    
    public Vector2 getPosition(){
        return position;
    }
    
    public WorldObject getWorldObject() {
        return worldObject;
    }
    
    public Vector2 getLocalPosition(){
        return position;
    }
    
    public Vector2 getScale(){
        return scale;
    }
    
    public int getChildCount(){
        return childs.size();
    }
    
    public Vector2 getLocalScale(){
        return scale;
    }
    
    public float getRotation(){
        return rotation;
    }
    
    public float getLocalRotation(){
        return rotation;
    }
    
    public void addChild(Transform child){
        if(!childs.contains(child))
            childs.add(child);
    }
    
    public void removeChild(Transform child){
        childs.remove(child);
    }
    
    public void translate(float x, float y){
        position.setX(position.getX()+ x);
        position.setY(position.getY()+ y);
        
        for(Transform child : this){
            child.translate(x, y);
        }
    }
    
    public void translate(double x, double y){
        position.setX(position.getX()+ x);
        position.setY(position.getY()+ y);
        
        for(Transform child : this){
            child.translate(x, y);
        }
    }

    @Override
    public Iterator<Transform> iterator()
    {
        return childs.iterator();
    }
}
