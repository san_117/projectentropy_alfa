package gameengine2018o.engine.world;

public class Vector2
{
    private double x;
    private double y;

    public Vector2(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    public double getX()
    {
        return x;
    }

    public void setX(double x)
    {
        this.x = x;
    }

    public double getY()
    {
        return y;
    }

    public void setY(double y)
    {
        this.y = y;
    }
    
    public void setY(float y)
    {
        this.y = y;
    }
    
    public String ToString(){
        return "X: " + getX() + "\nY: " + getY();
    }
}
