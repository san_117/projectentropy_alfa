package gameengine2018o.engine.world;

import components.Behaviour;
import gameengine2018o.engine.Collision;
import gameengine2018o.engine.Scene;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class WorldObject implements Iterable<Behaviour>
{
    protected Transform transform;
    protected Transform parent;
    
    protected boolean isInViewport;
    
    private ArrayList<Behaviour> components;
    
    private boolean isActive = true;

    public WorldObject(double x, double y, double width, double height)
    {      
        transform = new Transform(this,x,y,width,height,0);
        components = new ArrayList<>();
    }
    
    public WorldObject(double x, double y, double width, double height, Transform parent)
    {      
        transform = new Transform(this,x,y,width,height,0);
        components = new ArrayList<>();
        this.parent = parent;
        
        this.parent.addChild(transform); 
    }
    
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isIsActive() {
        return isActive;
    }
    
    public Transform getTransform() {
        return transform;
    }
    
    public void addComponent(Behaviour behaviour){
        
        components.add(behaviour);
        Scene.getCurrentScene().add(behaviour);
    }
    
    public void removeComponent(Behaviour behaviour){
        components.remove(behaviour);
    }
    
    public int componentsSize(){
        return components.size();
    }
    
    public void OnStart(ArrayList<Behaviour> scene_components){
        
        for(Behaviour c : components){
            if(!scene_components.contains(c))
                scene_components.add(c);
        }
        
        for(Transform child : transform){
            child.getWorldObject().OnStart(scene_components);
        }
    }
    
    public <T extends Behaviour> T getComponent(Class<T> type)
    {
        for(Behaviour b :components)
        {
            if(b.getClass().equals(type))
            {
                return type.cast(b);
            }
        }
        return null;
    }
    
    @Override
    public Iterator<Behaviour> iterator() {
        return components.iterator();
    }
    
    public void OnCollisionEnter(Collision coll){
        
    }
}
