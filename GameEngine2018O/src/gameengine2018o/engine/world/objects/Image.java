/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameengine2018o.engine.world.objects;

import gameengine2018o.engine.Converter;
import gameengine2018o.engine.Viewport;
import gameengine2018o.engine.window.WindowObject;
import gameengine2018o.engine.world.WorldObject;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author Pablo Rojas
 */
public class Image extends WorldObject
{
    private BufferedImage image;

    public Image(double x, double y, double width, double height, String pathname) throws IOException
    {
        super(x, y, width, height);
        this.image = ImageIO.read( new File(pathname) );
        
    }

//    @Override
//    public void paint(Graphics2D graphics, Viewport viewport, Dimension canvasDimension)
//    {
//        WindowObject object = Converter.toWindow(this, viewport, canvasDimension);
//        graphics.drawImage(this.image, object.getX(), object.getY(), object.getWidth(), object.getHeight(), null);
//    }

    
    
    
}
